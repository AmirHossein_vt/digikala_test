import React from 'react';
import {
  RenderNavigationContainer,
  SnackBarService,
} from './src/services';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import { LogBox } from 'react-native';
import configureStore from './src/store';
LogBox.ignoreAllLogs();
const store = configureStore();

const App = () => {
  return (
    <Provider store={store}>
      <SafeAreaProvider>
        <RenderNavigationContainer>
          <SnackBarService />
        </RenderNavigationContainer>
      </SafeAreaProvider>
    </Provider>
  );
};

export default App;
