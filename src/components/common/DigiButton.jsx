import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  Keyboard,
  ActivityIndicator,
  Animated,
} from 'react-native';
import PropTypes from 'prop-types';
import { DigiTouch } from './DigiTouch';
import { DigiText } from './DigiText';
import { Color } from '../../constants/theme';

const DigiButton = ({
  disabled,
  loading,
  shadowColor,
  onPress,
  style,
  type,
  title,
  width,
  height,
  mainColor,
  titleType,
  radius = 10,
  textStyle = {},
}) => {
  let children = null;
  let customStyle = {};
  const [internalLoading, setInternalLoading] = useState(false);
  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.91];
  const scale = animation.interpolate({ inputRange, outputRange });

  const onPressIn = () => {
    Animated.spring(animation, {
      toValue: 1,
      useNativeDriver: true,
    }).start();
  };
  const onPressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };
  useEffect(() => {
    setInternalLoading(loading);
  }, [loading]);

  const color =
    disabled || loading || internalLoading
      ? Color.gray
      : mainColor || Color.primary;

  switch (type) {
    default:
    case 'primary':
      children = (
        <Animated.View
          style={[
            s.mainButton,
            style && style,
            {
              transform: [{ scale }],
              backgroundColor: color,
              shadowColor: shadowColor ? shadowColor : color,
              borderRadius: radius,
              height,
              width,
            },
          ]}
        >
          <View style={[s.center, s.primaryStyle]}>
            {loading || internalLoading ? (
              <ActivityIndicator color={Color.light} size={'small'} />
            ) : (
              <React.Fragment>
                {title && (
                  <View style={s.title}>
                    <DigiText
                      type={titleType}
                      style={{ ...s.primaryTitle, ...textStyle }}
                    >
                      {title}
                    </DigiText>
                  </View>
                )}
              </React.Fragment>
            )}
          </View>
        </Animated.View>
      );
      break;
  }

  const turnOffLoading = () => {
    setInternalLoading(false);
  };

  const onButtonPress = () => {
    Keyboard.dismiss();
    setInternalLoading(true);
    onPress({ turnOffLoading });
  };

  return (
    <DigiTouch
      disabled={loading || internalLoading}
      loading={loading}
      onPress={onButtonPress}
      type="opacity"
      hasShadow={type === 'mini'}
      onPressIn={onPressIn}
      onPressOut={onPressOut}
      style={[customStyle]}
    >
      {children}
    </DigiTouch>
  );
};

const s = StyleSheet.create({
  center: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row-reverse',
  },
  primaryStyle: {
    flex: 1,
  },
  primaryIcon: {
    color: Color.light,
  },
  primaryTitle: {
    color: Color.light,
  },
  title: {
    flexDirection: 'column',
    width: '85%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    flexDirection: 'column',
    width: '15%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainButton: {
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
  },
});

DigiButton.propTypes = {
  type: PropTypes.oneOf(['primary', 'mini']),
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  title: PropTypes.string,
  icon: PropTypes.string,
  mainColor: PropTypes.string,
  titleType: PropTypes.string,
  iconSize: PropTypes.number,
  onPress: PropTypes.func,
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  width: PropTypes.number,
  height: PropTypes.number,
};

DigiButton.defaultProps = {
  style: {},
  type: 'primary',
  iconSize: 16,
  height: 45,
  width: 200,
  onPress: () => false,
  loading: false,
  disabled: false,
  titleType: 'heading4',
};

export { DigiButton };
