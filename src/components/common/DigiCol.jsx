import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

export const DigiCol = ({
  children,
  percentage,
  style,
  justify = 'center',
  align = 'center',
  height,
  width,
}) => {
  return (
    <View
      style={[
        { flexDirection: 'column' },
        percentage && { width: percentage + '%' },
        style && style,
        align && { alignItems: align },
        justify && { justifyContent: justify },
        width && { width },
        height && { height },
      ]}
    >
      {children}
    </View>
  );
};

DigiCol.propTypes = {
  align: PropTypes.oneOf(['center', 'flex-end', 'flex-start']),
  justify: PropTypes.oneOf([
    'center',
    'flex-end',
    'flex-start',
    'space-between',
    'space-around',
  ]),
};
