import React from 'react';
import { View } from 'react-native';

export const DigiDevider = ({
  width,
  height = 10,
  color,
  margin = 9,
  style = {},
}) => (
  <View
    style={{
      height: height,
      width: width,
      backgroundColor: color,
      borderRadius: 20,
      margin: margin,
      ...style,
    }}
  />
);
