import React, { useEffect, useState } from 'react';
import { TextInput } from 'react-native';
import { DigiRow } from './DigiRow';
import { Color } from '../../constants';

export const DigiInput = ({
  placeHolder = '',
  style = {},
  keyboardType = 'default',
  makePlaceHolderCenter = false,
  placeholderTextColor = Color.gray,
  maximumLenght = 1000,
  returnKeyType = 'done',
  injectValue = null,
  onKeyUp,
  numberOfLines = 1,
  editable = true,
  onChangeText = () => {},
}) => {
  const [value, setValue] = useState('');
  const [isTyping, setIsTyping] = useState({
    name: '',
    typing: false,
    typingTimeout: 0,
  });

  useEffect(() => {
    if (onKeyUp) {
      if (isTyping.typingTimeout) {
        clearTimeout(isTyping.typingTimeout);
      }
      setIsTyping({
        name: value,
        typing: false,
        typingTimeout: setTimeout(() => {
          onKeyUp(value);
        }, 500),
      });
    } else {
      onChangeText(value);
    }
  }, [value]);

  useEffect(() => {
    if (injectValue !== null) {
      setValue(injectValue);
    }
  }, [injectValue]);

  return (
    <DigiRow>
      <TextInput
        numberOfLines={numberOfLines}
        value={value}
        editable={editable}
        returnKeyType={returnKeyType}
        maxLength={maximumLenght}
        onChangeText={setValue}
        keyboardType={keyboardType}
        placeholderTextColor={placeholderTextColor}
        placeholder={placeHolder}
        style={[
          {
            width: '100%',
            fontFamily: 'Shabnam',

            borderBottomWidth: 0.5,
          },
          style && style,
          makePlaceHolderCenter && { textAlign: 'center' },
        ]}
      />
    </DigiRow>
  );
};
