import React, { useState, useEffect } from 'react';
import { Text, StyleSheet } from 'react-native';

const DigiLoading = (props) => {
  const [loading, setLoading] = useState(props.loading);

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 3000);
  }, []);
  return (
    <>
      {loading ? (
        <Text style={styles.loading}>Loading ...</Text>
      ) : (
        props.children
      )}
    </>
  );
};

export default DigiLoading;

const styles = StyleSheet.create({
  loading: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
