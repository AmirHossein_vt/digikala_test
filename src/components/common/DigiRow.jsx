import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

export const DigiRow = ({
  children,
  percentage,
  style = {},
  reverse = false,
  self = 'center',
  justify = 'center',
  align = 'center',
  height,
  width,
  onLayout = null,
}) => {
  return (
    <View
      onLayout={onLayout && onLayout}
      style={[
        { flexDirection: 'row' },
        percentage && { height: percentage + '%' },
        reverse && { flexDirection: 'row-reverse' },
        style && style,
        self && { alignSelf: self },
        justify && { justifyContent: justify },
        align && { alignItems: align },
        width && { width },
        height && { height },
      ]}
    >
      {children}
    </View>
  );
};

DigiRow.propTypes = {
  align: PropTypes.oneOf(['center', 'flex-end', 'flex-start']),
  justify: PropTypes.oneOf([
    'center',
    'flex-end',
    'flex-start',
    'space-between',
    'space-around',
  ]),
  reverse: PropTypes.bool,
};
