import React, { useEffect, useState } from 'react';
import { ActivityIndicator } from 'react-native';
import {
  RefreshControl,
  FlatList,
  ScrollView,
  View,
} from 'react-native';
import { Placeholder, PlaceholderLine, Fade } from 'rn-placeholder';
import { Color, DEVICE_WIDTH } from '../../constants';
import { DigiText } from './DigiText';
import { DigiCondition } from './DigiCondition';
import { DigiDevider } from './DigiDevider';

export const DigiScrollView = ({
  dataProvider = null,
  data,
  renderItems,
  hasRefresh = true,
  showsHorizontalScrollIndicator = false,
  dataFilterCondition = () => true,
  Content = null,
  reset = false,
  onResetEnd = () => {},
  SkeletonComponent = null,
  parameter,
}) => {
  const [customItems, setCustomItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [nextPageLoading, setNextPageLoading] = useState(false);
  const [nextPage, setNextPage] = useState(0);
  const [pages, setPages] = useState(1);
  const [refreshing, setRefreshing] = useState(false);

  const fetchNextPage = async () => {
    console.log(nextPage + ' eeee ' + pages);
    if (nextPage <= pages) {
      setNextPageLoading(true);
      const response = await dataProvider(nextPage);
      console.log(response, 'amir');
      const { current_page, total_pages } = response.pager.pager;
      const items = response[parameter];
      setCustomItems([...customItems, ...items]);
      setNextPage(current_page + 1);
      setPages(total_pages);
      setLoading(false);
      setNextPageLoading(false);
    } else {
      setNextPageLoading(false);
    }
  };

  useEffect(() => {
    (async () => {
      if (reset) {
        await refresh(false);
        onResetEnd();
      }
    })();
  }, [reset]);

  const refresh = async (withLoading = true) => {
    withLoading && setRefreshing(true);
    withLoading && setLoading(true);
    const response = await dataProvider(0);
    const { current_page, total_pages } = response.pager.pager;
    const items = response[parameter];
    setCustomItems([...items]);
    setNextPage(current_page + 1);
    setPages(total_pages);
    withLoading && setLoading(false);
    withLoading && setRefreshing(false);
  };

  if (Content) {
    return (
      <ScrollView
        showsHorizontalScrollIndicator={
          showsHorizontalScrollIndicator
        }
        style={{
          width: '100%',
        }}
      >
        <Content />
        <DigiDevider height={50} />
      </ScrollView>
    );
  }

  const globalRenderItems = () => {
    if (loading) {
      return () => (
        <View style={{ marginTop: 20 }}>
          <Placeholder Animation={Fade}>
            {SkeletonComponent ? (
              SkeletonComponent
            ) : (
              <PlaceholderLine
                width={parseInt(DEVICE_WIDTH / 4.5)}
                height={40}
                noMargin
                style={{ borderRadius: 100, alignSelf: 'center' }}
              />
            )}
          </Placeholder>
        </View>
      );
    }
    return renderItems;
  };

  useEffect(() => {
    if (!data && dataProvider) {
      setLoading(true);
      fetchNextPage();
    }
  }, []);

  const dataToRender = data ? data : customItems;
  const finalData = dataFilterCondition
    ? dataToRender.filter(dataFilterCondition)
    : dataToRender;

  if (!loading && finalData.length === 0) {
    return (
      <View style={{ marginTop: '35%' }}>
        <DigiText type={'mini'}>موردی یافت نشد !</DigiText>
      </View>
    );
  }

  return (
    <FlatList
      refreshControl={
        hasRefresh && (
          <RefreshControl
            colors={[Color.primary, Color.secondary]}
            refreshing={refreshing}
            onRefresh={refresh}
          />
        )
      }
      onMomentumScrollEnd={fetchNextPage}
      showsHorizontalScrollIndicator={showsHorizontalScrollIndicator}
      data={loading ? new Array(10) : finalData}
      keyExtractor={(item, index) => index}
      renderItem={globalRenderItems()}
      style={{
        marginTop: 10,
        width: '100%',
      }}
      ListFooterComponentStyle={{
        height: 110,
      }}
      ListFooterComponent={() => (
        <DigiCondition condition={nextPageLoading && !loading}>
          <ActivityIndicator color={Color.primary} size={25} />
        </DigiCondition>
      )}
    />
  );
};
