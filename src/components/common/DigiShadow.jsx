import React from 'react';
import { View } from 'react-native';
import { BoxShadow } from 'react-native-shadow';
import PropTypes from 'prop-types';

import { Color } from '../../constants/theme';

const DigiShadow = ({
  children,
  width,
  height,
  radius,
  shadowBlur,
  shadowColor,
  shadowOpacity,
  shadowX,
  shadowY,
  backgroundColor,
  style,
  containerStyle,
}) => {
  const myStyle = { overflow: 'hidden', position: 'relative' };
  return (
    <BoxShadow
      setting={{
        width,
        height,
        color: shadowColor,
        border: shadowBlur,
        radius,
        opacity: shadowOpacity,
        x: shadowX,
        y: shadowY,
        style: style,
      }}
    >
      <View
        style={[
          {
            width,
            height,
            borderRadius: radius,
            backgroundColor,
          },
          myStyle,
          containerStyle,
        ]}
      >
        {children}
      </View>
    </BoxShadow>
  );
};

DigiShadow.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  radius: PropTypes.number,
  shadowBlur: PropTypes.number,
  shadowColor: PropTypes.string,
  shadowOpacity: PropTypes.number,
  shadowX: PropTypes.number,
  shadowY: PropTypes.number,
  backgroundColor: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  containerStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
};
DigiShadow.defaultProps = {
  radius: 0,
  shadowBlur: 12,
  shadowColor: Color.dark,
  shadowOpacity: 0.12,
  shadowX: 0,
  shadowY: 4,
  backgroundColor: Color.light,
};

export { DigiShadow };
