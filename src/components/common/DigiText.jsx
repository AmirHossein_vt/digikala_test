import { putCommas, parseDigits } from '../../utils/common';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import TextTicker from 'react-native-text-ticker';
import { Color, dialogues } from '../../constants/index';
import { DigiCondition } from './DigiCondition';

const DigiText = ({
  type,
  unitTextStyle,
  children,
  style,
  light,
  dark,
  align,
  price = false,
  unit = price ? dialogues.common.toman : '',
  bold = false,
  numberOfLines = 100,
  tickerMode = false,
  underline = false,
}) => {
  const content = price ? parseDigits(putCommas(children)) : children;
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Text
        style={[
          s.span,
          s['mini'],
          light && s.light,
          dark && s.dark,
          align && { textAlign: align },
          unitTextStyle,
          !unitTextStyle && { fontSize: 12 },
        ]}
        allowFontScaling
      >
        {' '}
        {unit}
      </Text>
      <DigiCondition
        condition={tickerMode}
        elseAction={() => (
          <Text
            allowFontScaling
            numberOfLines={numberOfLines}
            style={[
              s.defaultText,
              s[type],
              light && s.light,
              dark && s.dark,
              align && { textAlign: align },
              style,
              bold && { fontFamily: 'Shabnam-Bold' },
              underline && { textDecorationLine: 'underline' },
            ]}
          >
            {content}
          </Text>
        )}
      >
        <TextTicker
          duration={3000}
          loop
          bounce={false}
          repeatSpacer={50}
          marqueeDelay={100}
          shouldAnimateTreshold={40}
          numberOfLines={1}
          isRTL={false}
          style={[
            s.defaultText,
            s[type],
            light && s.light,
            dark && s.dark,
            align && { textAlign: align },
            style,
            bold && { fontFamily: 'Shabnam-Bold' },
            underline && { textDecorationLine: 'underline' },
          ]}
        >
          {content}
        </TextTicker>
      </DigiCondition>
    </View>
  );
};

const s = StyleSheet.create({
  progressText: {
    fontSize: 16,
    color: Color.golden,
  },
  defaultText: {
    fontSize: 16,
    color: Color.secondary,
  },
  detailText: {
    color: Color.primary,
    fontSize: 14,
  },
  heading1: {
    color: Color.dark,
    fontSize: 22,
  },
  heading2: {
    color: Color.dark,
    fontSize: 20,
  },
  heading3: {
    color: Color.dark,
    fontSize: 18,
  },
  heading4: {
    color: Color.dark,
    fontSize: 16,
  },
  heading5: {
    color: Color.dark,
    fontSize: 14,
  },
  heading6: {
    color: Color.dark,
    fontSize: 12,
  },
  span: {
    color: Color.dark,
    fontSize: 13,
  },
  mini: {
    color: Color.gray,
    fontSize: 12,
  },
  paragraph1: {
    fontSize: 14,
  },
  paragraph2: {
    fontSize: 18,
  },
  light: {
    color: Color.light,
  },
  dark: {
    color: Color.light,
  },
  extraMini: {
    color: Color.gray,
    fontSize: 8,
  },
  dateExtraMini: {
    color: Color.gray,
    fontSize: 10,
  },
});

DigiText.propTypes = {
  type: PropTypes.oneOf([
    'heading1',
    'heading2',
    'heading3',
    'heading4',
    'heading5',
    'span',
    'paragraph1',
    'paragraph2',
    'mini',
  ]),
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  align: PropTypes.oneOf(['right', 'center', 'left', 'justify']),
};

DigiText.defaultProps = {
  style: { fontFamily: 'Shabnam' },
};

export { DigiText };
