import React from 'react';
import {
  TouchableNativeFeedback,
  Platform,
  View,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
} from 'react-native';

import PropTypes from 'prop-types';
import VibrationService from './../../services/vibrationService';

const DigiTouch = ({
  children = <></>,
  type,
  onPress,
  onLongPress = () => {},
  disabled,
  vibrateDuration = 40,
  activeOpacity = 0.85,
  hasShadow,
  ...props
}) => {
  let ButtonComponent = null,
    customProps = {};

  const onUserPress = (long = false) => {
    !long && onPress();
    long && onLongPress();
    VibrationService.tiny(vibrateDuration);
  };

  switch (type) {
    case 'opacity':
      ButtonComponent = TouchableOpacity;
      customProps = {
        activeOpacity,
      };
      break;
    case 'native':
      if (Platform.OS === 'android') {
        ButtonComponent = TouchableNativeFeedback;
        customProps = {
          background: TouchableNativeFeedback.SelectableBackground(),
        };
      } else {
        ButtonComponent = TouchableWithoutFeedback;
      }
      break;
    case 'highlight':
      ButtonComponent = TouchableHighlight;
      break;
    default:
    case 'without':
      ButtonComponent = TouchableWithoutFeedback;
      break;
  }

  return (
    <ButtonComponent
      onLongPress={() => !disabled && onUserPress(true)}
      disabled={disabled}
      onPress={() => !disabled && onUserPress(false)}
      {...{ ...customProps, ...props }}
    >
      <View style={hasShadow && { padding: 8 }}>{children}</View>
    </ButtonComponent>
  );
};

DigiTouch.propTypes = {
  type: PropTypes.oneOf([
    'opacity',
    'native',
    'highlight',
    'without',
  ]),
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
};

DigiTouch.defaultProps = {
  disabled: false,
  onPress: () => false,
};

export { DigiTouch };
