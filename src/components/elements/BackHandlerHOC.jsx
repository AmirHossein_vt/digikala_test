import React, { useEffect } from 'react';
import { BackHandler } from 'react-native';
import { navigate } from '../../services';
import { Header } from './../elements/Header';

export const BackHandlerHOC = ({ Component, name, headerMode }) => {
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backPressHandler,
    );
    return () => backHandler.remove();
  }, []);

  const backPressHandler = () => {
    switch (name) {
      case 'SplashPage':
      case 'LoginPage':
        return true;
      default:
        navigate('goBack');
    }
    return true;
  };

  return (
    <>
      {headerMode && (
        <Header
          name={name}
          {...headerMode}
          goBack={backPressHandler}
        />
      )}
      <Component />
    </>
  );
};
