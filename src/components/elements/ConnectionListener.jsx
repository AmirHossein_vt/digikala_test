import React, { useEffect } from 'react';
import NetInfo from '@react-native-community/netinfo';
import { dialogues } from '../../constants/index';
import { showToast } from '../../services/snackBarService';

export const ConnectionListener = () => {
  useEffect(() => {
    const netListenerUnsubscribe = NetInfo.addEventListener(
      ({ isConnected }) => {
        !isConnected &&
          showToast({
            text: dialogues.listners.connectionError,
            mode: 'error',
          });
      },
    );
    return () => {
      netListenerUnsubscribe();
    };
  }, []);

  return <></>;
};
