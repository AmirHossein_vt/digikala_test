import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import Config from 'react-native-config';
import { Color } from '../../constants';
import { DigiText } from '../common';
import VersionInfo from 'react-native-version-info';

export const EnvironmentBadge = () => {
  const [hidden, setHidden] = useState(false);
  const ENV_NAME = Config.ENVIRONMENT;

  useEffect(() => {
    setTimeout(() => {
      setHidden(true);
    }, 5000);
  }, []);

  if (Config.ENVIRONMENT !== 'PRODUCTION' && !hidden) {
    return (
      <View style={styles.style3}>
        <View style={styles.style2}>
          <DigiText
            style={{
              fontSize: 11,
              color:
                ENV_NAME === 'STAGING'
                  ? '#512da8'
                  : ENV_NAME === 'RC'
                  ? '#ff3d00'
                  : '#ffea00',
            }}
          >
            {ENV_NAME}
          </DigiText>
          <DigiText style={styles.style1}>
            {`DigikalaApp Version ( ${VersionInfo.appVersion} ) Build ( ${VersionInfo.buildVersion} ) ENV :`}
          </DigiText>
        </View>
      </View>
    );
  } else return <></>;
};

const styles = StyleSheet.create({
  style1: { color: Color.primary, fontSize: 10 },
  style2: {
    backgroundColor: Color.golden,
    height: 28,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row-reverse',
  },
  style3: {
    position: 'absolute',
    top: 0,
    elevation: 1,
    width: '100%',
  },
});
