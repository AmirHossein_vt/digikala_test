import React, { useState } from 'react';
import {
  StatusBar,
  View,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { DigiCondition } from '../common';
import { DEVICE_HEIGHT, Color } from '../../constants/index';
import { BackHandlerHOC } from './BackHandlerHOC';
import { SCREEN_OPTIONS } from '../../constants/schema';
import { ConnectionListener } from './ConnectionListener';

const Container = ({
  children,
  style,
  showUnderStatusBar,
  showStatus = true,
}) => {
  const insets = useSafeAreaInsets();
  StatusBar.setTranslucent(showUnderStatusBar ? true : false);
  return (
    <View
      style={[
        styles.containerBackground,
        style && style,
        {
          height: DEVICE_HEIGHT,
          display: showStatus ? 'flex' : 'none',
        },
        {
          paddingBottom: Math.max(insets.bottom, 16),
        },
      ]}
    >
      {children}
    </View>
  );
};

const Loading = ({ style, showStatus = true }) => {
  return (
    <View
      style={[
        {
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%',
          width: '100%',
          display: showStatus ? 'flex' : 'none',
        },
        style && style,
      ]}
    >
      <ActivityIndicator color={Color.primary} size={25} />
    </View>
  );
};

const HOC = (ComponentName, Component, props) => {
  const [appReady, setAppReady] = useState(false);
  const [globalLoading, setGlobalLoading] = useState(true);

  const {
    options: {
      showUnderStatusBar = false,
      headerMode = false,
      defaultLoading = false,
    },
  } = SCREEN_OPTIONS.find((screen) => screen.Name === ComponentName);

  const params = props?.route?.params || {};

  const hideLoading = () => setGlobalLoading(false);

  const componentTools = { hideLoading };

  requestAnimationFrame(() => {
    setAppReady(true);
    if (!defaultLoading) {
      hideLoading();
    }
  });

  return (
    <DigiCondition
      elseAction={() => <Loading />}
      condition={appReady}
    >
      <>
        <Loading showStatus={globalLoading} />
        <Container
          showStatus={!globalLoading}
          showUnderStatusBar={showUnderStatusBar}
        >
          <ConnectionListener />
          <BackHandlerHOC
            componentTools={componentTools}
            name={ComponentName}
            headerMode={headerMode}
            Component={() => (
              <Component
                {...params}
                navigation={props?.navigation}
                componentTools={componentTools}
                styles={styles}
              />
            )}
          />
        </Container>
      </>
    </DigiCondition>
  );
};

export { HOC };

const styles = StyleSheet.create({
  containerBackground: {
    backgroundColor: Color.white,
  },
});
