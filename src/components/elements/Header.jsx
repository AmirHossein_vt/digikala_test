import React from 'react';
import { View, StyleSheet } from 'react-native';
import { DigiText, DigiTouch } from './../common/index';
import { Color, DEVICE_WIDTH } from './../../constants/index';

export const Header = ({ showBackButton, goBack, name }) => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.columns}>
        <DigiText type={'heading5'}>{name}</DigiText>
      </View>

      {showBackButton && (
        <View style={styles.columns}>
          <DigiTouch
            style={styles.style1}
            type={'opacity'}
            onPress={goBack}
          >
            <DigiText style={styles.close}>X</DigiText>
          </DigiTouch>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: Color.white,
    width: DEVICE_WIDTH,
    flexDirection: 'row',
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    borderBottomColor: Color.dark,
    opacity: 1,
    position: 'relative',
  },
  style1: { padding: 10 },
  iconColor: {
    color: Color.dark,
  },
  close: { color: Color.red },
  textStyle: {
    color: Color.dark,
    alignSelf: 'center',
  },
  columns: {
    flexDirection: 'column',
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
