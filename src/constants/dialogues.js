export const dialogues = {
  api_call: {
    noMessageError: 'مشکل غیر منتظره ای پیش آمده',
    timeout: 'درخواست مورد نظر بیش تر از حد انتظار طول کشیده است',
    tooManyRequest: 'درخواست بیش از حد مجاز ، کمی بعد تلاش کنید',
    unAuth: 'نشست کاربری شما منقضی شده ، لطفا دوباره وارد شوید',
  },
  listners: {
    connectionError: 'اتصال به اینترنت برقرار نمیباشد',
  },
  Login: {
    username: 'username',
    password: 'password',
    login: 'login',
    success: 'you successfuly logined',
  },
  common: {
    toman: 'تومان',
  },
};
