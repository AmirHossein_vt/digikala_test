import { Home, Login, Search, Splash } from '../screens/';

export const NAVIGATION = {
  stacks: [
    {
      name: 'splash',
      tabBarStatus: false,
      screens: [
        {
          Name: 'SplashPage',
          Component: Splash,
        },
      ],
    },
    {
      name: 'auth',
      tabBarStatus: false,
      screens: [
        {
          Name: 'LoginPage',
          Component: Login,
        },
      ],
    },
    {
      name: 'app',
      tabBarStatus: true,
      screens: [
        {
          Name: 'HomePage',
          Component: Home,
        },
        {
          Name: 'SearchPage',
          Component: Search,
        },
      ],
    },
  ],
};

export const SCREEN_OPTIONS = [
  {
    Name: 'LoginPage',
    options: {},
  },
  {
    Name: 'HomePage',
    options: {
      headerMode: {
        showBackButton: false,
      },
    },
  },
  {
    Name: 'SearchPage',
    options: {
      headerMode: {
        showBackButton: true,
      },
    },
  },
];
