export const isUnAuth = (status) => {
  const result = status === 403 || status === 401;
  return result;
};

export const canRetry = (status) =>
  !status || parseInt((status + '')[0]) === 5;
