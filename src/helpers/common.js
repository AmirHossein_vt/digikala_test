import { UserModel } from '../models/user';
import { navigate } from '../services';
import { INIT_MOVIES } from '../store/types/movie';
import { getMovieRequest } from './endpoints';

export const initApplication = async (setProgress, dispatch) => {
  setProgress(0.2);
  await UserModel.loadLocalUserInfo();
  setProgress(0.5);

  if (UserModel.amILogin) {
    const { movies } = await getMovieRequest();
    setProgress(1);
    dispatch({ type: INIT_MOVIES, payload: movies });
    return navigate('app');
  }
  return navigate('auth');
};
