import { UserModel } from '../models/user';
import axiosService from '../services/axiosService';

export const loginRequest = async ({ username, password }) =>
  axiosService({
    method: 'POST',
    url: 'front-end/login/',
    data: { username, password },
  });

export const getGenresRequest = async (page) =>
  axiosService({
    method: 'GET',
    url: 'front-end/genres/',
    params: { rows: 10, page },
    headers: {
      Authorization: UserModel.getter('token'),
    },
  });

export const getMovieRequest = async () =>
  axiosService({
    method: 'GET',
    url: 'front-end/movies/',
    params: { rows: 60, page: 1 },
    headers: {
      Authorization: UserModel.getter('token'),
    },
  });
