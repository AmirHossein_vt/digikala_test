import { storageService } from '../services/storageService';

class UserModel {
  static userInfo = {};

  static async setter(user) {
    this.userInfo = user;
    await storageService.save('@USER', user);
  }

  static getter(param) {
    return this.userInfo?.[param] !== undefined ||
      this.userInfo?.[param] !== null
      ? this.userInfo?.[param]
      : '';
  }

  static get amILogin() {
    return this.userInfo?.token;
  }

  static loadLocalUserInfo = async function () {
    await storageService
      .load('@USER', true)
      .then((user) => {
        UserModel.setter(user);
      })
      .catch(() => {
        UserModel.setter({});
      });
  };
}

export { UserModel };
