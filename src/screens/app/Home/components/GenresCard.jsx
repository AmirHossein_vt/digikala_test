import React from 'react';
import { StyleSheet } from 'react-native';
import {
  DigiRow,
  DigiText,
  DigiTouch,
} from '../../../../components/common';
import { Color, DEVICE_WIDTH } from '../../../../constants';
import { navigate } from '../../../../services';

export default ({ name }) => {
  const goToSearch = () => {
    navigate('SearchPage', {
      injectedFilter: {
        parameter: 'genres',
        value: name,
      },
    });
  };

  return (
    <DigiTouch type={'opacity'} onPress={goToSearch}>
      <DigiRow height={40} style={styles.main}>
        <DigiText style={styles.style3}>{name}</DigiText>
      </DigiRow>
    </DigiTouch>
  );
};

const styles = StyleSheet.create({
  main: {
    borderWidth: 1,
    borderRadius: 100,
    borderStyle: 'dashed',
    width: DEVICE_WIDTH * 0.7,
    marginVertical: 15,
  },
  style3: {
    color: Color.dark,
  },
});
