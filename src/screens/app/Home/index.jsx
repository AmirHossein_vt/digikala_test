import React from 'react';
import { DigiScrollView } from '../../../components/common';
import { HOC } from '../../../components/elements';
import { getGenresRequest } from '../../../helpers/endpoints';
import GenresCard from './components/GenresCard';

const HomePage = () => {
  return (
    <DigiScrollView
      parameter={'genres'}
      dataProvider={getGenresRequest}
      renderItems={({ item, index }) => (
        <GenresCard key={index} {...item} />
      )}
    />
  );
};

export const Home = () => HOC('HomePage', HomePage);
