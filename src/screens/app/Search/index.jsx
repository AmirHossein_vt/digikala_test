import React, { useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import {
  DigiCondition,
  DigiInput,
  DigiScrollView,
  DigiText,
  DigiTouch,
} from '../../../components/common';
import { HOC } from '../../../components/elements';
import { Color, DEVICE_WIDTH } from '../../../constants';
import { useSelector } from 'react-redux';

const SearchPage = ({ injectedFilter }) => {
  const [param, setParam] = useState('');
  const [customFilter, setCustomFilter] = useState(null);
  const {
    movie: { movies },
  } = useSelector((state) => state);

  useEffect(() => {
    setCustomFilter(injectedFilter);
  }, [injectedFilter]);

  const finalData = (
    customFilter
      ? movies.filter((item) =>
          item[customFilter.parameter].includes(customFilter.value),
        )
      : movies
  ).filter((item) => item.title.includes(param));

  const emptyFilter = () => {
    setCustomFilter(null);
  };

  return (
    <View style={styles.style1}>
      <DigiCondition condition={customFilter}>
        <DigiText style={styles.style2}>Custom filters :</DigiText>
        <View style={styles.main}>
          <DigiText style={styles.style2}>
            {customFilter?.parameter} : {customFilter?.value}
          </DigiText>
          <DigiTouch
            style={styles.style5}
            type={'opacity'}
            onPress={emptyFilter}
          >
            <DigiText style={styles.style4}>X</DigiText>
          </DigiTouch>
        </View>
      </DigiCondition>

      <View style={styles.style6}>
        <DigiInput
          onKeyUp={setParam}
          placeHolder={'search param'}
          style={styles.style7}
        />
      </View>
      <DigiScrollView
        data={finalData}
        renderItems={({ item, index }) => (
          <DigiText
            underline
            type={'heading5'}
            align={'center'}
            tickerMode
            style={styles.text}
          >
            {index} - {item.title}
          </DigiText>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    borderRadius: 40,
    borderWidth: 1,
    borderColor: Color.dark,
    flexDirection: 'row',
    paddingHorizontal: 8,
    marginVertical: 50,
  },
  text: {
    marginVertical: 10,
    color: Color.lightBlue,
    alignSelf: 'center',
  },
  style1: {
    width: DEVICE_WIDTH,
    justifyContent: 'center',
    alignItems: 'center',
  },
  style2: { color: Color.dark },
  style3: { padding: 5 },
  style4: { color: Color.red },
  style5: { padding: 5 },
  style6: { width: '80%' },
  style7: { alignSelf: 'center' },
});

export const Search = (props) => HOC('SearchPage', SearchPage, props);
