import React, { useState } from 'react';
import { View, KeyboardAvoidingView, StyleSheet } from 'react-native';
import {
  DigiButton,
  DigiCol,
  DigiInput,
  DigiRow,
} from '../../../components/common';
import { DEVICE_WIDTH, dialogues } from '../../../constants';
import { Color } from '../../../constants';
import { HOC } from '../../../components/elements';
import { showToast } from '../../../services';
import { UserModel } from '../../../models/user';
import { loginRequest } from '../../../helpers/endpoints';
import { useDispatch } from 'react-redux';
import { initApplication } from '../../../helpers';

const LoginPage = () => {
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const dispatch = useDispatch();

  const loginUser = async ({ turnOffLoading }) => {
    try {
      const { token } = await loginRequest({
        username,
        password,
      });
      UserModel.setter({ token });
      initApplication(() => {}, dispatch);
      showToast({
        text: dialogues.Login.success,
        mode: 'success',
      });
    } finally {
      turnOffLoading();
    }
  };

  return (
    <KeyboardAvoidingView behavior={'padding'} style={styles.style3}>
      <DigiCol style={styles.style2} justify={'flex-end'}>
        <DigiRow percentage={70}>
          <DigiCol
            height={'100%'}
            width={'80%'}
            justify={'flex-start'}
          >
            <DigiInput
              onChangeText={setUsername}
              returnKeyType="next"
              maximumLenght={20}
              placeHolder={dialogues.Login.username}
            />
            <DigiInput
              onChangeText={setPassword}
              returnKeyType="next"
              maximumLenght={20}
              placeHolder={dialogues.Login.password}
            />
            <View style={styles.style1}>
              <DigiButton
                onPress={loginUser}
                disabled={!username || !password}
                width={DEVICE_WIDTH * 0.9}
                textStyle={styles.style5}
                style={styles.style4}
                mainColor={Color.primary}
                title={dialogues.Login.login}
              />
            </View>
          </DigiCol>
        </DigiRow>
      </DigiCol>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  style1: {
    position: 'absolute',
    bottom: 30,
  },
  style2: {
    backgroundColor: Color.white,
    height: '100%',
  },
  style3: { flex: 1 },
  style4: {
    marginTop: 10,
    marginBottom: 15,
  },
  style5: { color: Color.white },
});

export const Login = () => HOC('LoginPage', LoginPage);
