import React, { useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Color, DEVICE_WIDTH } from '../../../constants';
import { useIsFocused } from '@react-navigation/native';
import * as Progress from 'react-native-progress';
import { useDispatch } from 'react-redux';
import { initApplication } from '../../../helpers';
import { EnvironmentBadge } from './../../../components/elements/EnvironmentBadge';

export const Splash = () => {
  const [progress, setProgress] = useState(0);
  const isFocused = useIsFocused();
  const dispatch = useDispatch();

  useEffect(() => {
    isFocused && initApplication(setProgress, dispatch);
  }, []);

  return (
    <View style={styles.container}>
      <EnvironmentBadge />

      <View style={styles.style1}>
        <Progress.Bar
          color={Color.darkYellow}
          unfilledColor={Color.tooLightGray}
          borderWidth={0}
          progress={progress}
          width={DEVICE_WIDTH / 2}
          style={{ marginTop: 50 }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    backgroundColor: Color.white,
  },
  style1: { transform: [{ rotateY: '180deg' }] },
});
