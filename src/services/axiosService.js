import axios from 'axios';
import { BASE_URL } from '../constants/environments';
import { showToast } from './snackBarService';
import { canRetry } from '../helpers';
import { dialogues } from '../constants';

const axiosService = axios.create({
  headers: {
    'Cache-Control': 'no-cache',
    'Content-Type': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
  },
  baseURL: BASE_URL,
  timeout: 10000,
});

axiosService.interceptors.response.use(
  async (response) => {
    const { status, message } = response.data;
    if (parseInt(status.toString()[0]) === 2) {
      return response.data.data;
    }

    if (canRetry(status)) {
      return new Promise((resolve, reject) => {
        showToast({
          text: message ?? dialogues.noMessageError,
          mode: 'error',
          actions: [
            {
              title: dialogues.retry,
              onPress: () => resolve(axiosService.request(e.config)),
            },
          ],
          duration: 30000,
          didFinished: () => {
            reject(Promise.reject(message));
          },
        });
      });
    } else {
      showToast({
        text: message,
        mode: 'error',
        duration: 10000,
      });
      return Promise.reject(message);
    }
  },
  async (e) => {
    showToast({
      text: dialogues.api_call.noMessageError,
      mode: 'error',
      duration: 10000,
    });
    return Promise.reject(dialogues.api_call.noMessageError);
  },
);

export default axiosService;
