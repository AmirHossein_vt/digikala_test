import React, { createRef } from 'react';
import { v4 as randomUIID } from 'uuid';
import { enableScreens } from 'react-native-screens';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import { NAVIGATION } from '../constants/schema';
import { CommonActions } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { View } from 'react-native';
import { Color } from '../constants';

enableScreens();

const NestedStack = createNativeStackNavigator();
const RootStack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
export const isReadyRef = createRef();
export const navigationRef = createRef();

export const RenderNavigationContainer = ({ children }) => {
  const RenderScreens = ({ screens }) => (
    <NestedStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      {screens.map((screen) => (
        <NestedStack.Screen
          key={randomUIID}
          name={screen.Name}
          component={screen.Component}
        />
      ))}
    </NestedStack.Navigator>
  );

  const RenderTabs = ({ screens }) => (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarIcon: ({ focused }) => {
          return (
            <View
              style={{
                height: 9,
                width: 9,
                borderRadius: 30,
                backgroundColor: focused
                  ? Color.primary
                  : Color.white,
              }}
            />
          );
        },
        tabBarActiveTintColor: Color.primary,
        tabBarInactiveTintColor: Color.gray,
      }}
    >
      {screens.map((screen) => (
        <Tab.Screen
          key={randomUIID}
          name={screen.Name}
          component={screen.Component}
        />
      ))}
    </Tab.Navigator>
  );

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => {
        isReadyRef.current = true;
      }}
    >
      <RootStack.Navigator>
        {NAVIGATION.stacks.map((stack) => (
          <RootStack.Screen
            name={stack.name}
            component={() => {
              if (stack.tabBarStatus) {
                return <RenderTabs screens={stack.screens} />;
              } else {
                return <RenderScreens screens={stack.screens} />;
              }
            }}
            options={{ headerShown: false }}
          />
        ))}
      </RootStack.Navigator>
      {children}
    </NavigationContainer>
  );
};

export const navigate = (name, params) => {
  if (isReadyRef.current && navigationRef.current) {
    if (name === 'goBack') {
      navigationRef.current.dispatch(CommonActions.goBack());
    } else {
      navigationRef.current.navigate(name, params);
    }
  } else {
    alert('app is not ready for navigation');
  }
};

export const stackReset = ({ stack, params }) => {
  navigationRef.current.dispatch(
    CommonActions.reset({
      index: 1,
      routes: [
        {
          name: stack,
          params: params,
        },
      ],
    }),
  );
};
