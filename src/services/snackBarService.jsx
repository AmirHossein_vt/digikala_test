import React from 'react';
import {
  DigiCol,
  DigiCondition,
  DigiRow,
  DigiText,
  DigiTouch,
} from '../components/common';
import { View, StyleSheet } from 'react-native';
import { Color, DEVICE_WIDTH } from '../constants';
import Toast from 'react-native-toast-message';

let showToast = (object) => [object];

const SnackBarService = () => {
  showToast = ({
    text,
    mode = 'success',
    actions = [],
    duration = 2000,
    didFinished = () => {},
  }) => {
    Toast.show({
      type: mode,
      text1: text,
      props: { actions, mode },
      visibilityTime: duration,
      onHide: didFinished,
    });
  };

  const RenderToast = ({
    text1,
    props: { actions = [], mode },
    props,
  }) => {
    const hasActions = actions?.length > 0;

    let config = '';
    switch (mode) {
      case 'success':
        config = {
          color: Color.green,
          icon: 'tik',
        };
        break;
      case 'warn':
        config = {
          color: Color.darkYellow,
          icon: 'alert',
        };
        break;
      case 'info':
        config = {
          color: Color.lightBlue,
          icon: 'alert',
        };
        break;
      case 'error':
        config = {
          color: Color.red,
          icon: 'alert',
        };
        break;
    }

    return (
      <View style={[{ backgroundColor: config.color }, styles.main]}>
        <DigiRow width={'100%'} justify={'flex-end'} align={'center'}>
          <DigiText
            type={'heading5'}
            style={[
              styles.style1,
              {
                marginBottom: hasActions ? 10 : 0,
              },
            ]}
            numberOfLines={10}
          >
            {text1}
          </DigiText>
        </DigiRow>
        <DigiCondition condition={hasActions}>
          <DigiRow width={'100%'} justify={'flex-end'}>
            {actions.map(
              (action) =>
                action && (
                  <DigiCol
                    percentage={100 / actions.length}
                    align="center"
                    justify="center"
                  >
                    <DigiTouch
                      type={'opacity'}
                      onPress={() => {
                        Toast.hide();
                        setTimeout(() => {
                          action.onPress();
                        }, 300);
                      }}
                      style={styles.style2}
                    >
                      <DigiText style={styles.style3}>
                        {action.title}
                      </DigiText>
                    </DigiTouch>
                  </DigiCol>
                ),
            )}
          </DigiRow>
        </DigiCondition>
      </View>
    );
  };
  const toastConfig = {
    success: RenderToast,
    info: RenderToast,
    error: RenderToast,
    warn: RenderToast,
  };

  return (
    <Toast config={toastConfig} ref={(ref) => Toast.setRef(ref)} />
  );
};

const styles = StyleSheet.create({
  main: {
    minHeight: 50,

    width: DEVICE_WIDTH * 0.9,
    alignSelf: 'center',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'flex-end',
    shadowColor: Color.dark,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    padding: 10,
  },
  style1: {
    color: Color.white,
    marginRight: 10,
    width: '90%',
  },
  style2: {
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Color.white,
    padding: 5,
  },
  style3: { color: Color.white },
});

export { SnackBarService, showToast };
