import { Vibration } from 'react-native';

class VibrationService {
  static tiny(time = 40) {
    Vibration.vibrate(time);
  }

  static stopVibrate() {
    Vibration.cancel();
  }

  static patternVibrate(pattern) {
    Vibration.vibrate(pattern, true);
  }
}

export default VibrationService;
