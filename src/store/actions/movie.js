import { INIT_MOVIES } from '../types/movie';
export const initMyMovies = (movies) => {
  return {
    type: INIT_MOVIES,
    payload: movies,
  };
};
