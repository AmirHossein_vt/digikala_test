import { INIT_MOVIES } from '../types/movie';
const initialState = {
  movies: [],
};
const movieReducer = (state = initialState, action) => {
  switch (action.type) {
    case INIT_MOVIES:
      return {
        ...state,
        movies: action.payload,
      };
    default:
      return state;
  }
};
export default movieReducer;
