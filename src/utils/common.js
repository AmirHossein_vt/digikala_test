export function* chunk(arr, n) {
  for (let i = 0; i < arr.length; i += n) {
    yield arr.slice(i, i + n);
  }
}
export const parseDigits = (number) => {
  const faNum = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
  return String(number).replace(/[0-9]/g, (w) => {
    return faNum[+w];
  });
};

export const putCommas = (number) => {
  if (!number) {
    return number;
  }
  if (typeof number === 'undefined') {
    return number;
  }
  if (typeof number === 'number') {
    number = number.toString();
  }
  if (number.includes('.')) {
    const numbers = number.split('.');
    return (
      numbers[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',') +
      '.' +
      numbers[1]
    );
  }
  return number.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

export const toEnglish = (str) => {
  let persianNumbers = [
      /۰/g,
      /۱/g,
      /۲/g,
      /۳/g,
      /۴/g,
      /۵/g,
      /۶/g,
      /۷/g,
      /۸/g,
      /۹/g,
    ],
    arabicNumbers = [
      /٠/g,
      /١/g,
      /٢/g,
      /٣/g,
      /٤/g,
      /٥/g,
      /٦/g,
      /٧/g,
      /٨/g,
      /٩/g,
    ];
  if (typeof str === 'string') {
    for (var i = 0; i < 10; i++) {
      str = str
        .replace(persianNumbers[i], i)
        .replace(arabicNumbers[i], i);
    }
  }
  return str;
};
